#include <iostream>
#include <thread>

static int totalRocketsLaunchedIntoSpace;

static bool bigRedAbortButton = false;

void LaunchRocketFromOffsiteRocketPad();
void AbortionComplete();

void main() 
{
	std::cout << "\t\t\n\nWelcome to mars mission control\n\n";

	std::cout << "\nOur main thread ID at Mission Control is " << std::this_thread::get_id() << std::endl;
	std::cout << "\nTo get started launching rockets, ";
	system("pause");

	int missionControlLaunchedRockets = 0;
	missionControlLaunchedRockets++;
	totalRocketsLaunchedIntoSpace++;
	std::cout << "\nWe are launching mission control rocket number " << missionControlLaunchedRockets << " from Mission control.\n";
	std::cout << "\nWe are launching total base rocket number " << totalRocketsLaunchedIntoSpace << " from Mission control.\n";

	std::thread startOffsiteRocketLaunchThread(LaunchRocketFromOffsiteRocketPad);

	std::cout << "\nTo abort the rockets launch remotely, press the big red button. (Any button will do)\n ";
	system("pause > nul");
	bigRedAbortButton = true;
	std::cout << "\nYou have Pressed the button!!\n";
	system("pause");

	startOffsiteRocketLaunchThread.join();

	std::thread youHaveAborted(AbortionComplete);

	youHaveAborted.join();

}

void LaunchRocketFromOffsiteRocketPad() 
{
	system("pause");
	using namespace std::literals::chrono_literals;

	std::cout << "\nwelcome to the remote offsite launchpad.\n";

	int offsiteLaunchRockets = 0;

	std::cout << "\nOur offsite launch pad thread ID is " << std::this_thread::get_id() << std::endl;

	while(bigRedAbortButton != true)
	{
		std::cout << "\nlaunching rocket #" << ++offsiteLaunchRockets << " from offsite launch pad";

		std::cout << "\nwe are launching total base rocket number " << ++totalRocketsLaunchedIntoSpace << " from Mission control.\n";

		std::this_thread::sleep_for(3s);
	}
}

void AbortionComplete() 
{
	std::cout << "You have aborted the rockets, but there are some that have launched.\n";
	system("pause");
}